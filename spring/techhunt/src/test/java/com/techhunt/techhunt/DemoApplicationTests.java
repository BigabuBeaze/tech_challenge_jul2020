package com.techhunt.techhunt;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Order;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.techhunt.techhunt.model.Employee;
import com.techhunt.techhunt.service.EmployeeService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@TestMethodOrder(OrderAnnotation.class)
class DemoApplicationTests {
	@LocalServerPort
	int localPort = 4200;
    @Autowired
    private EmployeeService employeeService;
	
	@Test
	@Order(1)
	public void test1AddEmployeeSuccess() throws URISyntaxException {
		
        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:"+localPort+"/api/save-employee/";
        URI uri = new URI(baseUrl);
		
		Employee employee = new Employee();
		employee.setEmployee_id(9999);
		employee.setLogin("testLogin");
		employee.setName("Tester A");
		employee.setSalary(3200.00f);

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");      
 
        HttpEntity<Employee> request = new HttpEntity<>(employee, headers);
         
        ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);
         
        //Verify request succeed
		Assertions.assertEquals(200, result.getStatusCodeValue());
		List<Employee> tempList = employeeService.getEmployeeByID(employee);
		Assertions.assertEquals(employee.getEmployee_id(), tempList.get(0).getEmployee_id());
		Assertions.assertEquals(employee.getLogin(), tempList.get(0).getLogin());
		Assertions.assertEquals(employee.getName(), tempList.get(0).getName());
		Assertions.assertEquals(employee.getSalary(), tempList.get(0).getSalary());
	}

	@Test
	@Order(2)
	public void test2GetEmployeeSuccess() throws URISyntaxException {
		int employeeId = 9999;
        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:"+localPort+"/api/employee/" + employeeId;
        URI uri = new URI(baseUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");      
 
		HttpEntity<Employee> request = new HttpEntity<>(headers);
		
		Employee employee = new Employee();
		employee.setEmployee_id(9999);
		employee.setLogin("testLogin");
		employee.setName("Tester A");
		employee.setSalary(3200.00f);
		
		ParameterizedTypeReference<List<Employee>> responseType = new ParameterizedTypeReference<List<Employee>>() {};
         
		ResponseEntity<List<Employee>> result = restTemplate.exchange(uri, HttpMethod.GET, request, responseType);
		List<Employee> resultList = result.getBody();

        //Verify request succeed
		Assertions.assertEquals(200, result.getStatusCodeValue());
		Assertions.assertEquals(1, resultList.size());
		Assertions.assertEquals(employee.getEmployee_id(), resultList.get(0).getEmployee_id());
		Assertions.assertEquals(employee.getLogin(), resultList.get(0).getLogin());
		Assertions.assertEquals(employee.getName(), resultList.get(0).getName());
		Assertions.assertEquals(employee.getSalary(), resultList.get(0).getSalary());
	}

	@Test
	@Order(3)
	public void test3UpdateEmployeeSuccess() throws URISyntaxException {
		int employeeId = 9999;
        final String baseUrl = "http://localhost:"+localPort+"/api/update-employee/" + employeeId;
        URI uri = new URI(baseUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		
		Employee employee = new Employee();
		employee.setEmployee_id(9999);
		employee.setLogin("testLogin");
		employee.setName("Tester A_UPDATED");
		employee.setSalary(3201.50f);

        HttpEntity<Employee> request = new HttpEntity<>(employee, headers);
		
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.PATCH, request, String.class);

        //Verify request succeed
		Assertions.assertEquals(200, result.getStatusCodeValue());
		Assertions.assertEquals("success", result.getBody());
	}

	@Test
	@Order(4)
	public void test4DeleteEmployeeSuccess() throws URISyntaxException {
		int employeeId = 9999;
        final String baseUrl = "http://localhost:"+localPort+"/api/delete-employee/" + employeeId;
        URI uri = new URI(baseUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		
		Employee employee = new Employee();
		employee.setEmployee_id(9999);
		employee.setLogin("testLogin");
		employee.setName("Tester A_UPDATED");
		employee.setSalary(3201.50f);

        HttpEntity<Employee> request = new HttpEntity<>(employee, headers);
		
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.DELETE, request, String.class);

        //Verify request succeed
		Assertions.assertEquals(200, result.getStatusCodeValue());
	}


}
