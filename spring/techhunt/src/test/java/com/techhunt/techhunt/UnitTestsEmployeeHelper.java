package com.techhunt.techhunt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.techhunt.techhunt.model.Employee;
import com.techhunt.techhunt.helper.EmployeeHelper;
import com.techhunt.techhunt.helper.ReturnHelper;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class UnitTestsEmployeeHelper {

    private static final java.util.logging.Logger log = java.util.logging.Logger.getLogger(EmployeeHelper.class.getName());
    @Test
    public void emptyCSVRead() {

        String csvString = "employee_id,login,name,salary\n";

        String check = "returnhelper-There are no records in the CSV file.";
        List<Employee> employeeList = new ArrayList<Employee>();
        ReturnHelper rh = new ReturnHelper(check, employeeList);

        InputStream stream = new ByteArrayInputStream(csvString.getBytes(StandardCharsets.UTF_8));

        ReturnHelper checkRH = EmployeeHelper.csvToEmployees(stream);

        assertEquals(rh.getCheck(), checkRH.getCheck());
    }

    @Test
    public void tooMuchHeadersCSVRead() {

        String csvString = "employee_id,login,name,salary,something\n";

        String check = "returnhelper-The CSV file does not have exactly 4 headers: 'employee_id', 'login', 'name' and 'salary'";
        List<Employee> employeeList = new ArrayList<Employee>();
        ReturnHelper rh = new ReturnHelper(check, employeeList);

        InputStream stream = new ByteArrayInputStream(csvString.getBytes(StandardCharsets.UTF_8));

        ReturnHelper checkRH = EmployeeHelper.csvToEmployees(stream);

        assertEquals(rh.getCheck(), checkRH.getCheck());
    }

    @Test
    public void noHeaderCSVRead() {

        String csvString = "";

        String check = "returnhelper-The CSV file does not have exactly 4 headers: 'employee_id', 'login', 'name' and 'salary'";
        List<Employee> employeeList = new ArrayList<Employee>();
        ReturnHelper rh = new ReturnHelper(check, employeeList);

        InputStream stream = new ByteArrayInputStream(csvString.getBytes(StandardCharsets.UTF_8));

        ReturnHelper checkRH = EmployeeHelper.csvToEmployees(stream);
        
        assertEquals(rh.getCheck(), checkRH.getCheck());
    }
}