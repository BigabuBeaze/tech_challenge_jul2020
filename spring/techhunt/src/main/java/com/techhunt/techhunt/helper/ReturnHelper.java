package com.techhunt.techhunt.helper;
import java.util.List;

import com.techhunt.techhunt.model.Employee;

public final class ReturnHelper {
    private final String check;
    private final List<Employee> employeeList;

    public ReturnHelper(String check, List<Employee> employeeList) {
      this.check = check;
      this.employeeList = employeeList;
    }

    public List<Employee> getEmployeeList(){
      return employeeList;
    }

    public String getCheck(){
      return check;
    }
}