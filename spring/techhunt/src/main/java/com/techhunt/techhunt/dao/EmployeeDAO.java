package com.techhunt.techhunt.dao;

import java.util.List;
import com.techhunt.techhunt.model.Employee;

public interface EmployeeDAO {
    public boolean saveEmployee(Employee employee);
    public List<Employee> getEmployees();
    public List<Employee> getEmployeesFilter(String minS, String maxS, String offs, String limit, String sField, String sType);
    public boolean deleteEmployee(Employee employee);
    public List<Employee> getEmployeeByID(Employee employee);
    public boolean updateEmployee(Employee employee);
    public boolean checkEmployeeLogin(String login, int id);
    public boolean checkEmployeeLoginUpload(String login, int id);
    public boolean saveMultEmployees(List<Employee> employeeList);
}