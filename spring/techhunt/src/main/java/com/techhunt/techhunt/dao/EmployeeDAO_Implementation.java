package com.techhunt.techhunt.dao;

import java.util.List;
  
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.techhunt.techhunt.model.Employee;

@Repository
public class EmployeeDAO_Implementation implements EmployeeDAO{
    @Autowired
    private SessionFactory sessionFactory;

    private static final java.util.logging.Logger log = java.util.logging.Logger.getLogger(EmployeeDAO_Implementation.class.getName());

    @Override
    public boolean saveEmployee(Employee employee) {
        boolean status = false;
        try {
            sessionFactory.getCurrentSession().saveOrUpdate(employee);
            status=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public List<Employee> getEmployees() {
        Session currentSession = sessionFactory.getCurrentSession();
        Query<Employee> query = currentSession.createQuery("from Employee", Employee.class);
        List<Employee> list=query.getResultList();
        return list;
    }

    @Override  
    public boolean deleteEmployee(Employee employee) {
        boolean status=false;
        try {
            sessionFactory.getCurrentSession().delete(employee);
            status=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }
  
    @Override  
    public List<Employee> getEmployeeByID(Employee employee) {
        Session currentSession = sessionFactory.getCurrentSession();
        Query<Employee> query = currentSession.createQuery("from Employee where employee_id=:employee_id", Employee.class);
        query.setParameter("employee_id", employee.getEmployee_id());
        List<Employee> list=query.getResultList();
        return list;
    }
    
    @Override  
    public List<Employee> getEmployeesFilter(String minS, String maxS, String offs, String limit, String sField, String sType) {
        float minSal = Float.parseFloat(minS);
        float maxSal = Float.parseFloat(maxS);
        Session currentSession = sessionFactory.getCurrentSession();
        Query<Employee> query = currentSession.createQuery("from Employee where salary between :minS and :maxS order by :sField", Employee.class);
        if (sType == "a"){
            query = currentSession.createQuery("from Employee where salary between :minS and :maxS order by :sField ASC", Employee.class);
        } else if (sType == "d"){
            query = currentSession.createQuery("from Employee where salary between :minS and :maxS order by :sField DESC", Employee.class);
        }
        query.setParameter("minS", minSal);
        query.setParameter("maxS", maxSal);
        query.setParameter("sField", sField);
        query.setFirstResult(Integer.parseInt(offs));
        query.setMaxResults(Integer.parseInt(limit));
        List<Employee> list=query.getResultList();
        return list;
    }
  
    @Override  
    public boolean updateEmployee(Employee employee) {
        boolean status = false;
        try {
            sessionFactory.getCurrentSession().update(employee);
            status=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public boolean checkEmployeeLogin(String login, int id){
        boolean status = false;
        Session currentSession = sessionFactory.getCurrentSession();
        Query<Employee> query=currentSession.createQuery("from Employee where login=:login", Employee.class);  
        query.setParameter("login", login);
        List<Employee> list=query.getResultList();
        if (list.size() > 0) {
            if (list.size() == 1 && list.get(0).getEmployee_id() == id){
                status = false;
            } else {
                status = true;
            }
        }
        sessionFactory.getCurrentSession().clear();
        return status;
    }
    
    @Override
    public boolean checkEmployeeLoginUpload(String login, int id){
        boolean status = false;
        Session currentSession = sessionFactory.getCurrentSession();
        Query<Employee> query=currentSession.createQuery("from Employee where login=:login", Employee.class);  
        query.setParameter("login", login);
        List<Employee> list=query.getResultList();
        if (list.size() > 0) {
            if (list.size() == 1 && list.get(0).getEmployee_id() == id){
                status = false;
            } else {
                status = true;
            }
        }
        sessionFactory.getCurrentSession().clear();
        return status;
    }

    @Override
    public boolean saveMultEmployees(List<Employee> employeeList){
        boolean status = false;
        for (Employee employee : employeeList) {
            try {
                sessionFactory.getCurrentSession().saveOrUpdate(employee);
                status=true;
            } catch (Exception e) {
                e.printStackTrace();
                status = false;
            }
        }
        return status;
    }
}