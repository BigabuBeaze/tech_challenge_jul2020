package com.techhunt.techhunt.service;

import java.util.List;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.techhunt.techhunt.dao.EmployeeDAO;
import com.techhunt.techhunt.helper.EmployeeHelper;
import com.techhunt.techhunt.helper.ReturnHelper;
import com.techhunt.techhunt.model.Employee;

@Service
@Transactional
public class EmployeeService_Implementation implements EmployeeService{
    private static final java.util.logging.Logger log = java.util.logging.Logger.getLogger(EmployeeService_Implementation.class.getName());
    @Autowired  
    private EmployeeDAO employeedao;

    @Override
    public boolean saveEmployee(Employee employee) {
        return employeedao.saveEmployee(employee);
    }

    @Override
    public List<Employee> getEmployees() {
        return employeedao.getEmployees();
    }

    @Override
    public List<Employee> getEmployeesFilter(String minS, String maxS, String offs, String limit, String sField, String sType) {
        return employeedao.getEmployeesFilter(minS, maxS, offs, limit, sField, sType);  
    }

    @Override
    public boolean deleteEmployee(Employee employee) {
        return employeedao.deleteEmployee(employee);
    }

    @Override
    public List<Employee> getEmployeeByID(Employee employee) {  
        return employeedao.getEmployeeByID(employee);  
    }

    @Override
    public String updateEmployee(Employee employee) {
        boolean check = false;
        if (employeedao.checkEmployeeLogin(employee.getLogin(), employee.getEmployee_id())) {
            return "login exists - " + employee.getLogin();
        } else {
            check = employeedao.updateEmployee(employee);
            if (check) {
                return "success";
            } else {
                return "unknown error";
            }
        }
    }

    @Override
    public String saveMultEmployees(List<Employee> employeeList) {
        for (Employee employee : employeeList) {
            if (employeedao.checkEmployeeLoginUpload(employee.getLogin(), employee.getEmployee_id())){
                return "login: " + employee.getLogin();
            }
        }
        if (employeedao.saveMultEmployees(employeeList)){
            return "success";
        } else {
            return "error";
        }
    }

    @Override
    public String saveCSV(MultipartFile file) {
        try {
            ReturnHelper rh = EmployeeHelper.csvToEmployees(file.getInputStream());
            if (rh.getCheck().equals("ok")){
                List<Employee> employeeList = rh.getEmployeeList();
                return saveMultEmployees(employeeList);
            } else {
                return rh.getCheck();
            }
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }
}