package com.techhunt.techhunt.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
  
@Entity
@Table(name="employee")
public class Employee {
    @Id
    private int employee_id;
    private String login;
    private String name;
    private Float salary;
    
    public int getEmployee_id() {
        return employee_id;
    }
    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Float getSalary() {
        return salary;
    }
    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public String getCSVString() {
        return employee_id + "," + login + "," + name + "," + salary;
    }
}