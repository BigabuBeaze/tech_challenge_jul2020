package com.techhunt.techhunt.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import com.techhunt.techhunt.model.Employee;

public class EmployeeHelper {
    public static String TYPE = "text/csv";
    static String[] HEADERs = { "employee_id", "login", "name", "salary" };
    private static final java.util.logging.Logger log = java.util.logging.Logger.getLogger(EmployeeHelper.class.getName());

    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        } else {
            return true;
        }
    }

    public static ReturnHelper csvToEmployees(InputStream is) {
        String check = "";
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            CSVParser csvParser = new CSVParser(fileReader,
                CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {
            
            List<Employee> employees = new ArrayList<Employee>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            if (csvParser.getHeaderNames() == null) {
                check = "returnhelper-The CSV file has missing headers.";
                return new ReturnHelper(check, employees);
            }
            if (csvParser.getHeaderNames().size() != 4) {
                check = "returnhelper-The CSV file does not have exactly 4 headers: 'employee_id', 'login', 'name' and 'salary'";
                return new ReturnHelper(check, employees);
            }
            List<String> headerNames = csvParser.getHeaderNames();
            for (int i = 0; i < headerNames.size(); i++){
                if (!headerNames.get(i).equalsIgnoreCase(HEADERs[i])) {
                    check = "returnhelper-The CSV file has headers which aren't the following: 'employee_id', 'login', 'name' and 'salary'";
                    return new ReturnHelper(check, employees);
                }
            }
            if (csvParser.getRecords().size() > 0) {
                for (CSVRecord csvRecord : csvRecords) {
                    Employee employee = new Employee();
                    if (csvRecord.get(HEADERs[0]).substring(0, 1).equals("#")) {
                        continue;
                    }
                    if (!csvRecord.get(HEADERs[0]).isEmpty()) {
                        try {
                            employee.setEmployee_id(Integer.parseInt(csvRecord.get(HEADERs[0])));
                        } catch (Exception e) {
                            check = "returnhelper-There is an incorrect value for one of the IDs in the CSV.";
                            employees.clear();
                            return new ReturnHelper(check, employees);
                        }
                    } else {
                        check = "returnhelper-There is a missing value for one of the IDs in the CSV.";
                        employees.clear();
                        return new ReturnHelper(check, employees);
                    }
                    if (!csvRecord.get(HEADERs[1]).isEmpty()) {
                        employee.setLogin(csvRecord.get(HEADERs[1]));
                    } else {
                        check = "returnhelper-There is a missing value for one of the logins in the CSV.";
                        employees.clear();
                        return new ReturnHelper(check, employees);
                    }
                    if (!csvRecord.get(HEADERs[2]).isEmpty()) {
                        employee.setName(csvRecord.get(HEADERs[2]));
                    } else {
                        check = "returnhelper-There is a missing value for one of the names in the CSV.";
                        employees.clear();
                        return new ReturnHelper(check, employees);
                    }
                    if (!csvRecord.get(HEADERs[3]).isEmpty()) {
                        int indexOfDecimal = HEADERs[3].indexOf(".");
                        if(HEADERs[3].substring(indexOfDecimal).length() <= 2) {
                            try {
                                employee.setSalary(Float.parseFloat(csvRecord.get(HEADERs[3])));
                            } catch (Exception e){
                                check = "returnhelper-There is an incorrect value for one of the salaries in the CSV.";
                                employees.clear();
                                return new ReturnHelper(check, employees);
                            }
                            if (employee.getSalary() < 0){
                                check = "returnhelper-The salary for the employee with id: " + employee.getEmployee_id() + " is negative.";
                                employees.clear();
                                return new ReturnHelper(check, employees);
                            }
                        } else {
                            check = "returnhelper-The salary for the employee with id: " + employee.getEmployee_id() + " is not properly formatted.";
                            employees.clear();
                            return new ReturnHelper(check, employees);
                        }
                    } else {
                        check = "returnhelper-There is a missing value for one of the salaries in the CSV.";
                        employees.clear();
                        return new ReturnHelper(check, employees);
                    }
                    log.info("Employee List size: " + employees.size());
                    if (employees.size() > 0){
                        for (Employee employeeCheck : employees){
                            log.info("CHECK: " + employee.getEmployee_id() + " AGAINST " + employeeCheck.getEmployee_id());
                            if (employee.getEmployee_id() == employeeCheck.getEmployee_id()){
                                check = "returnhelper-The ID: " + employee.getEmployee_id() + " is duplicated within the CSV.";
                                employees.clear();
                                return new ReturnHelper(check, employees);
                            } else if (employee.getLogin().equals(employeeCheck.getLogin())){
                                check = "returnhelper-The login: " + employee.getLogin() + " is duplicated within the CSV.";
                                employees.clear();
                                return new ReturnHelper(check, employees);
                            }
                        }
                    }

                    employees.add(employee);
                }

                check = "ok";
                return new ReturnHelper(check, employees);
            } else {
                check = "returnhelper-There are no records in the CSV file.";
                employees.clear();
                return new ReturnHelper(check, employees);
            }

        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}