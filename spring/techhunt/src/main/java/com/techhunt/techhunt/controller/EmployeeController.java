package com.techhunt.techhunt.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.HttpStatus;

import com.techhunt.techhunt.model.Employee;
import com.techhunt.techhunt.service.EmployeeService;
import com.techhunt.techhunt.helper.EmployeeHelper;
import com.techhunt.techhunt.message.ResponseMessage;

@RestController
@CrossOrigin(origins={"http://localhost:4200", "http://localhost:8080"})
@RequestMapping(value="/api")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    
    @PostMapping("/save-employee")
    public boolean saveEmployee(@RequestBody Employee employee) {  
         return employeeService.saveEmployee(employee);
    }

    @PostMapping("/save-mult-employees")
    public String saveMultipleEmployees(@RequestBody List<Employee> employeeList) {
        return employeeService.saveMultEmployees(employeeList);
    }
      
    @GetMapping("/employee-list")
    public List<Employee> allEmployees() {
         return employeeService.getEmployees();
    }

    @GetMapping("/employee-list-filter")
    public List<Employee> allEmployeesFilter(@RequestParam String minS, @RequestParam String maxS, @RequestParam String offs, 
                                                @RequestParam String limit, @RequestParam String sField, @RequestParam String sType) {  
         return employeeService.getEmployeesFilter(minS, maxS, offs, limit, sField, sType);
    }
      
    @DeleteMapping("/delete-employee/{employee_id}")
    public boolean deleteEmployee(@PathVariable("employee_id") int employee_id,Employee employee) {
        employee.setEmployee_id(employee_id);
        return employeeService.deleteEmployee(employee);
    }
  
    @GetMapping("/employee/{employee_id}")
    public List<Employee> getEmployeeByID(@PathVariable("employee_id") int employee_id,Employee employee) {
        employee.setEmployee_id(employee_id);
         return employeeService.getEmployeeByID(employee);
    }
      
    @PatchMapping("/update-employee/{employee_id}")
    public String updateEmployee(@RequestBody Employee employee,@PathVariable("employee_id") int employee_id) {
        employee.setEmployee_id(employee_id);
        return employeeService.updateEmployee(employee);
    }

    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";
        String check = "";
        if (EmployeeHelper.hasCSVFormat(file)) {
            try {
                check = employeeService.saveCSV(file);

                if (check.equals("success")){
                    message = "Uploaded the file successfully: " + file.getOriginalFilename();
                    return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
                } else if (check.equals("error")){
                    message = "An unknown error has occured while trying to add records into the database.";
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseMessage(message));
                } else if (check.substring(0, 13).equals("returnhelper-")) {
                    message = check.substring(13);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
                } else {
                    message = "The login: " + check + " has been found to have duplicates in the database.";
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
                }
                // return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage("temp"));
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }

        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }
}