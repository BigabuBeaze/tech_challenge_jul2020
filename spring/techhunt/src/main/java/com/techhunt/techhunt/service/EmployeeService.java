package com.techhunt.techhunt.service;

import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import com.techhunt.techhunt.model.Employee;

public interface EmployeeService {
    public boolean saveEmployee(Employee employee);  
    public List<Employee> getEmployees();
    public List<Employee> getEmployeesFilter(String minS, String maxS, String offs, String limit, String sField, String sType);
    public boolean deleteEmployee(Employee employee);  
    public List<Employee> getEmployeeByID(Employee employee);  
    public String updateEmployee(Employee employee);   
    public String saveMultEmployees(List<Employee> employeeList); 
    public String saveCSV(MultipartFile file);
}