import { EmployeeService } from './../employee.service';
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { Employee } from '../employee';
import { Observable, Subject } from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { faEdit, faTrash, faSearch } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit, OnDestroy, AfterViewInit{
  faEdit = faEdit;
  faTrash = faTrash;
  faSearch = faSearch;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  employeeArray: any[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  employees: Observable<Employee[]>;
  employee: Employee = new Employee();
  deleteMessage = false;
  employeeList: any;
  isUpdated = false;
  updateResponse = '';
  updateMessage = '';

  isFilter = false;
  filterMessage = '';
  filters: string[];
  fMinS = '';
  fMaxS = '';
  fOffs = '0';
  fLimit = '30';
  fSField = 'employee_id';
  fSType = 'a';

  filterField = 0;
  filterType = 'asc';

  constructor(private employeeService: EmployeeService, private activatedRoute: ActivatedRoute, private router: Router,
              private toastr: ToastrService) { }


  employeeFilterForm = new FormGroup({
    minSalary: new FormControl('' , [Validators.required, Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')]),
    maxSalary: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')]),
    offset: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
    limit: new FormControl('', [Validators.required, Validators.min(30), Validators.max(30)]),
    sortField: new FormControl('', Validators.required),
    sortType: new FormControl('', Validators.required)
  });

  employeeUpdateForm = new FormGroup({
    employee_id: new FormControl(),
    employee_login: new FormControl('', Validators.required),
    employee_name: new FormControl('', Validators.required),
    employee_salary: new FormControl('', [Validators.required, Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')])
  });

  employeeDeleteForm = new FormGroup({
    employee_id: new FormControl(),
    employee_login: new FormControl(),
    employee_name: new FormControl(),
    employee_salary: new FormControl()
  });

  get employeeId(): number{
    return this.employeeUpdateForm.get('employee_id').value;
  }
  get employeeLogin(): string{
    return this.employeeUpdateForm.get('employee_login').value;
  }
  get employeeName(): string{
    return this.employeeUpdateForm.get('employee_name').value;
  }
  get employeeSalary(): number{
    return this.employeeUpdateForm.get('employee_salary').value;
  }

  changeisUpdate(): void{
    this.isUpdated = false;
  }
  changeisDelete(): void{
    this.deleteMessage = false;
  }

  ngOnInit(): void {
    this.isUpdated = false;

    this.activatedRoute.paramMap.subscribe(params => {
      this.filters = [params.get('minS'), params.get('maxS'), params.get('offs'), params.get('limit'), params.get('sField'), params.get('sType')];
    });

    for (let x of this.filters) {
      if (!x) {
        this.isFilter = false;
        break;
      } else {
        this.isFilter = true;
      }
    }

    if (this.isFilter){
      switch (this.filters[4]) {
        case 'employee_id':
          this.filterField = 0;
          break;
        case 'login':
          this.filterField = 1;
          break;
        case 'name':
          this.filterField = 2;
          break;
        case 'salary':
          this.filterField = 3;
          break;
      }
      switch (this.filters[5]) {
        case 'a':
          this.filterType = 'asc';
          break;
        case 'd':
          this.filterType = 'desc';
          break;
      }

      this.dtOptions = {
        pageLength: 6,
        stateSave: true,
        lengthMenu: [[6, 16, 20, -1], [6, 16, 20, 'All']],
        processing: true,
        autoWidth: true,
        order: [this.filterField, this.filterType]
      };
    }
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
    if (this.isFilter){
      this.employeeService.getEmployeeListF(this.filters[0], this.filters[1], this.filters[2],
                                              this.filters[3], this.filters[4], this.filters[5]).subscribe(data => {
                                                this.toastr.info('Loading filtered employees into table...');
                                                this.employees = data;
                                                this.renderTable();
                                              }, error => console.log(error));
    }
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  loadAllEmployees(): void {
    this.loadEmployees().then(value => {
      this.renderTable();
    });
  }

  async loadEmployees(): Promise<void> {
    if (!this.isFilter){
      this.toastr.info('Loading all employees, please wait...');
      return new Promise((resolve, reject) => {
          this.employeeService.getEmployeeList().subscribe(data => {
          this.employees = data;
          resolve();
        }, error => {
          this.toastr.error('Something went wrong with loading the employees.');
          reject(error);
        });
      });
      } else {
        this.employeeService.getEmployeeListF(this.filters[0], this.filters[1], this.filters[2],
                                                this.filters[3], this.filters[4], this.filters[5]).subscribe(data => {
                                                  this.employees = data;
                                                  this.renderTable();
                                                }, error => console.log(error));
      }
  }

  deleteEmployee(id: number): void{
    this.employeeService.deleteEmployee(id)
    .subscribe(
      data => {
        this.deleteMessage = true;
        if (!this.isFilter){
          this.toastr.info('Reloading table, please wait...');
          this.employeeService.getEmployeeList().subscribe(data => {
            this.employees = data;
            this.renderTable();
          }, error => console.log(error));
        } else {
          this.employeeService.getEmployeeListF(this.filters[0], this.filters[1], this.filters[2],
                                                  this.filters[3], this.filters[4], this.filters[5]).subscribe(data => {
                                                    this.toastr.info('Reloading table, please wait...');
                                                    this.employees = data;
                                                    this.renderTable();
                                                  }, error => console.log(error));
        }
      },
    error => console.log(error));
  }

  updateEmployee(id: number): void{
    this.employeeService.getEmployee(id)
    .subscribe(data => {
      this.employeeList = data;
    },
    error => console.log(error));
  }

  updEmpl(employeeId: number, employeeName: string, employeeLogin: string, employeeSalary: number): void{
    this.employee = new Employee();
    this.employee.employee_id = employeeId;
    this.employee.name = employeeName;
    this.employee.login = employeeLogin;
    this.employee.salary = employeeSalary;

    this.employeeService.updateEmployee(this.employee.employee_id, this.employee).subscribe(
      data => {
        this.updateResponse = data;
        if (this.updateResponse === 'success'){
        this.isUpdated = true;
        this.updateMessage = 'Employee updated successfully!';
        if (!this.isFilter){
          this.toastr.info('Reloading table, please wait...');
          this.employeeService.getEmployeeList().subscribe(data => {
            this.employees = data;
            this.renderTable();
          }, error => console.log(error));
        } else {
          this.employeeService.getEmployeeListF(this.filters[0], this.filters[1], this.filters[2],
                                                  this.filters[3], this.filters[4], this.filters[5]).subscribe(data => {
                                                    this.toastr.info('Reloading table, please wait...');
                                                    this.employees = data;
                                                    this.renderTable();
                                                  }, error => console.log(error));
        }
      } else {
      this.isUpdated = true;
      this.updateMessage = 'There was an error with updating the employee: ' + this.updateResponse;
      }
    }, error => console.log(error));
  }

  filterEmployees(): void {
    if (this.fMinS === '' || this.fMaxS === '' || this.fOffs === '' || this.fLimit === '' || this.fSField === ''
        || this.fSType === '' || this.fLimit != '30'){
          this.filterMessage = 'Something seems to be wrong with the search queries, please double check the values.';
    } else {
      this.router.navigate(['/view-employee/' + this.fMinS + '/' + this.fMaxS + '/' + this.fOffs + '/' + this.fLimit +
                            '/' + this.fSField + '/' + this.fSType]);
    }
  }

  clearFilters(): void {
    this.router.navigate(['/view-employee']);
  }

  renderTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
      this.toastr.success('Table loaded.');
    });
  }
}
