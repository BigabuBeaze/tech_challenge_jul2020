export class Employee {

  employee_id: number;
  login: string;
  name: string;
  salary: number;
}
