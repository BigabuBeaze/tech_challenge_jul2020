import { EmployeeListComponent } from './../employee-list/employee-list.component';
import { Component, ViewChild, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { Employee } from '../employee';
import { Papa } from 'ngx-papaparse';
import { Observable, Subject } from 'rxjs';
import * as fileSaver from 'file-saver';
import { _ParseAST } from '@angular/compiler';
import { DataTableDirective } from 'angular-datatables';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  employee: Employee = new Employee();
  submitted = false;
  selectedCSVFileName = '';
  isCSVValid = false;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  jsonpre = '';
  csv = '';
  employeesTemp: Employee[] = [];

  warningMessage = '';
  successMessage = '';
  errorMessage = '';
  restResponse = '';

  idCheck = false;
  loginCheck = false;

  fileUploading = false;

  downloadSample(): void{
    this.jsonpre = '[{"employee_id":"1","login":"Lauri","name":"Amerman","salary":"1980"},{"employee_id":"2","login":"Rebbecca","name":"Bellon","salary":"1977"}]';
    this.csv = this.papa.unparse(this.jsonpre);
    let blob = new Blob([decodeURIComponent(encodeURI(this.csv))]);
    fileSaver.saveAs(blob, 'sample.csv');
  }

  constructor(private employeeService: EmployeeService, private papa: Papa, private toastr: ToastrService) {}

  // LOAD CSV FILE FROM INPUT
  async fileChangeListener($event: any): Promise<void> {
    this.toastr.info('Attempting to read file...');
    if (!this.fileUploading) {
      this.fileUploading = true;
      this.errorMessage = '';
      const files = $event.srcElement.files;

      if (files !== null && files !== undefined && files.length > 0) {
        return new Promise((resolve, reject) => {
          this.selectedCSVFileName = files[0].name;
          const reader: FileReader = new FileReader();
          reader.readAsText(files[0]);
          reader.onload = e => {

            const csv = reader.result;
            if (csv) {
              const results = this.papa.parse(csv as string, { header: false });
              console.log(results);

              if (results.data[0].length !== 4){
                this.errorMessage = 'It seems that the number of columns in the CSV is incorrect. Please make sure there are only 4 columns.';
                this.toastr.error('Reading failed, please refer to the error message.');
                this.employeesTemp.length = 0;
                this.fileUploading = false;
                reject();
              } else {
                // VALIDATE PARSED CSV FILE
                if (results !== null && results !== undefined && results.data !== null &&
                  results.data !== undefined && results.data.length > 0 && results.errors.length === 0) {
                  this.isCSVValid = true;

                  // PERFORM OPERATIONS ON PARSED CSV
                  let csvTableHeader = results.data[0];

                  let csvTableData = [...results.data.slice(1, results.data.length)];

                  if (!this.checkCSVHeaders(csvTableHeader)){
                    this.errorMessage = 'It seems that the headers for the CSV are incorrect. Please make sure they are as per the instructions before uploading.';
                    this.toastr.error('Reading failed, please refer to the error message.');
                    this.employeesTemp.length = 0;
                    reject();
                  } else {
                    for (let x = 0; x < csvTableData.length; x++){
                      if (csvTableData[x][0] != null && csvTableData[x][2] == null
                        && csvTableData[x][2] == null && csvTableData[x][3] == null){
                        // Some bug with checking the csv
                        continue;
                      }
                      else{
                        if (csvTableData[x][0].substring(0, 1) !== '#') {
                          this.employee = new Employee();
                          this.employee.employee_id = csvTableData[x][0];
                          this.employee.login = csvTableData[x][1];
                          this.employee.name = csvTableData[x][2];
                          this.employee.salary = csvTableData[x][3];
                          if (this.employee.salary > 0){
                            this.employee.salary = Math.round(this.employee.salary * 100) / 100;
                          }
                          if (!this.employee.employee_id || !this.employee.login
                            || !this.employee.name || !this.employee.salary){
                              this.errorMessage = 'It seems that there\'s a record with a blank value in it after this row with ' +
                              'employee_id: ' + this.employeesTemp[this.employeesTemp.length - 1].employee_id + '.';
                              this.toastr.error('Reading failed, please refer to the error message.');
                              this.employeesTemp.length = 0;
                              reject();
                            }
                          else if (!this.checkSalary(this.employee.salary)){
                            this.errorMessage = 'It seems that an employee with the ID: ' + this.employee.employee_id + ' has a negative salary.';
                            this.toastr.error('Reading failed, please refer to the error message.');
                            this.employeesTemp.length = 0;
                            reject();
                          } else {
                            for (let i of this.employeesTemp){
                              if (i.employee_id === this.employee.employee_id){
                                this.idCheck = true;
                                break;
                              }
                              if (i.login === this.employee.login) {
                                this.loginCheck = true;
                                break;
                              }
                            }
                            if (!this.idCheck && !this.loginCheck) {
                              this.employeesTemp.push(this.employee);
                            } else {
                              if (this.idCheck) {
                                this.errorMessage = 'It seems like there is a duplicate of the id: ' + this.employee.employee_id + ' within the CSV.';
                                this.toastr.error('Reading failed, please refer to the error message.');
                                this.employeesTemp.length = 0;
                                reject();
                              } else if (this.loginCheck) {
                                this.errorMessage = 'It seems like there is a duplicate of the login: ' +
                                                      this.employee.login + ' in the CSV.';
                                this.toastr.error('Reading failed, please refer to the error message.');
                                this.employeesTemp.length = 0;
                                reject();
                              }
                              this.idCheck = false;
                              this.loginCheck = false;
                              this.employeesTemp.length = 0;
                              resolve();
                            }
                          }
                        }
                      }
                    }
                  }
                  if (!this.errorMessage) {
                    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                      dtInstance.destroy();
                      this.dtTrigger.next();
                    });
                    this.toastr.success('Upload completed!');
                  }
                } else {
                  for (let x of results.errors) {
                    console.log( 'Error Parsing CSV File: ', x.message);
                  }
                }
                this.fileUploading = false;
              }
             } else {
                this.errorMessage = 'It seems that the CSV file is empty.';
                this.toastr.error('Reading failed, please refer to the error message.');
                this.employeesTemp.length = 0;
                this.fileUploading = false;
                reject();
            }
          };
        });
      } else {
        console.log('No File Selected');
        this.toastr.error('Error: no file was selected.');
        this.employeesTemp.length = 0;
      }
      this.fileUploading = false;
    } else {
      this.errorMessage = 'A file is currently being uploaded, please wait a moment.';
      this.toastr.error('Reading failed, please refer to the error message.');
      this.employeesTemp.length = 0;
    }
  }

  checkSalary(salary: number): boolean {
    if (salary < 0.0) {
      return false;
    } else {
      return true;
    }
  }

  checkCSVHeaders(headers: string[]): boolean {
    const checker = ['Employee_ID', 'Login', 'Name', 'Salary'];
    for (let h = 0; h < headers.length; h++){
      if (headers[h].toLowerCase() !== checker[h].toLowerCase()){
        return false;
      }
    }
    return true;
  }

  addEmployees(): void{
    if (this.employeesTemp.length > 0){
      this.employeeService.createMultEmployees(this.employeesTemp)
      .subscribe(data => {
        this.restResponse = data;
        if (this.restResponse === 'success') {
          this.successMessage = 'Employees have been successfully added!';
          this.employeesTemp = [];
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          });
        } else if (this.restResponse === 'error') {
          this.errorMessage = 'There seems to be an unknown error with adding the employees...';
        } else {
          this.errorMessage = 'An employee already exists with the ' + this.restResponse;
        }
      }, error => console.log(error));
    } else {
      this.warningMessage = 'There\'s an issue with adding the employees, do check if the details are correct before proceeding.';
    }
  }

  ngOnInit(): void{
    this.submitted = false;
    this.dtOptions = {
      pageLength: 6,
      stateSave: true,
      lengthMenu: [[6, 16, 20, -1], [6, 16, 20, 'All']],
      processing: true
    };
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}
