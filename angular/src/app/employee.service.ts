import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private baseUrl = 'http://localhost:8080/api';

  constructor(private http: HttpClient) { }

  getEmployeeList(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + '/employee-list');
  }

  getEmployeeListF(minS: string, maxS: string, offs: string, limit: string, sField: string, sType: string): Observable<any> {
    return this.http.get(`${this.baseUrl}` + '/employee-list-filter?minS=' + minS + '&maxS=' + maxS + '&offs=' +
                          offs + '&limit=' + limit + '&sField=' + sField + '&sType=' + sType);
  }

  createEmployee(employee: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + '/save-employee', employee);
  }

  createMultEmployees(employees: object[]): Observable<any> {
    return this.http.post(`${this.baseUrl}` + '/save-mult-employees', employees, { responseType: 'text' });
  }

  deleteEmployee(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/delete-employee/${id}`, { responseType: 'text' });
  }

  getEmployee(id: number): Observable<object> {
    return this.http.get(`${this.baseUrl}/employee/${id}`);
  }

  updateEmployee(id: number, value: any): Observable<any> {
    return this.http.patch(`${this.baseUrl}/update-employee/${id}`, value,  { responseType: 'text' });
  }
}
