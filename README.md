**SWE Tech Hunt July 2020** 

**Yu Mun Pan**

My attempt for the Tech Hunt challenge.

To load the projects:

| MySQL Dependency for springboot |
| ------ |

    - Have a local MYSQL database with an account that could access it.
        - The springboot project currently has the database as 'techhunt_demo_employee'
        - The springboot project has an account with the user 'techHuntDemo' with no password
        
    - If any of the details would be different, do head over to tech_challenge_jul2020\spring\techhunt\src\main\resources\application.properties to alter the db. settings
    
    - A MySQL dump for the base structure of the database is within the root folder of the project
----------------------------------------------------------------------------------------------------------------------------------------
| Springboot Project |
| ------ |

    
    - To start the springboot project (Self package jar):
        - Make sure to have jdk1.8 and maven installed (My maven is version 3.6.3)
        
        - Open a command prompt and navigate to tech_challenge_jul2020/spring/techhunt
        
        - Enter the command 'mvn clean package' to generate a jar file in the target folder of tech_challenge_jul2020/spring/techhunt
        
        - Find the location of the jar file inside the target folder of the springboot project
            - tech_challenge_jul2020/spring/techhunt/target/techhunt-0.0.1-SNAPSHOT.jar
            
        - Open a command prompt and enter the command 'java -jar {location of jar}'
            - e.g. 'java -jar tech_challenge_jul2020/spring/techhunt/target/techhunt-0.0.1-SNAPSHOT.jar'
    
    - To start the springboot project (pre-packaged jar) :
        - Open a command prompt and navigate to the tech_challenge_jul2020/spring
        
        - Open a command prompt and enter the command 'java -jar {location of jar}'
            - e.g. 'java -jar tech_challenge_jul2020/spring/techhunt-0.0.1-SNAPSHOT.jar'
            
            
        - APIs will be at http://localhost:8080/api
            - POST:
                - /save-employee
                - /save-mult-employees
                - /upload
            - GET: 
                - /employee-list
                - /employee-list-filter
                - /employee/{employee_id}
            - DELETE:
                - /delete-employee/{employee_id}
            - PATCH: 
                - /update-employee/{employee_id}
    
----------------------------------------------------------------------------------------------------------------------------------------
| Angular Project |
| ------ |

    - To start the angular project:
        - Make sure to have Angular CLI and NodeJS installed (My current Angular CLI is version 10.0.1)
        
        - Find the location of the angular folder within the project
            - tech_challenge_jul2020/angular
            
        - Open a command prompt and navigate to that location
        
        - Enter the command 'npm install' to install all dependencies of the project
        
        - Enter the command 'ng serve' to start the service
        
        - Web page will be at: http://localhost:4200

Notes for the project(s): 

| Assumptions |
| ------ |
    
    - Employee ID is in: int
    - Login, name is in: string
    - Salary is in:      float

| Attempts |
| ------ |

    Of the 5 user stories, 1 - 4 were attempted.
    
| Credits |
| ------ |

    credits:
        https://www.javatpoint.com/angular-spring-crud-example 
            - base codes
        https://bezkoder.com/spring-boot-upload-csv-file/ 
            - upload csv file for rest
        https://code.tutsplus.com/tutorials/how-to-upload-and-download-csv-files-with-angularjs--cms-31489 
            - upload csv for angular
        https://howtodoinjava.com/spring-boot2/testing/junit5-with-spring-boot2/ 
            - getting started with junit 5
        https://github.com/spring-cloud/spring-cloud-netflix/issues/1777 
            - PATCH for junit with resttemplates

